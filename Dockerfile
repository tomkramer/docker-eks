FROM python:3

RUN pip install awscli --upgrade --user
RUN curl --silent --location "https://github.com/weaveworks/eksctl/releases/download/latest_release/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
RUN mv /tmp/eksctl /usr/local/bin

RUN curl -o kubectl https://amazon-eks.s3-us-west-2.amazonaws.com/1.13.7/2019-06-11/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mkdir -p $HOME/bin && cp ./kubectl $HOME/bin/kubectl && echo 'export PATH=$HOME/bin:$PATH' >> ~/.bashrc

RUN pip3 install --upgrade --user awscli && echo 'export PATH=$HOME/.local/bin:$PATH' >> ~/.bashrc

WORKDIR /root

ENTRYPOINT [ "/bin/bash" ]